package main

import (
	"os"
	"sync"
	"time"

	"log/slog"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New(fiber.Config{
		DisableStartupMessage: true,
	})

	// example:
	// curl -X POST http://127.0.0.1:9999/api/write/10
	app.Post("/api/write/:number", writeNumber)

	if err := app.Listen(":" + os.Getenv("PORT")); err != nil {
		slog.Error("http server: failed to listen and serve",
			"error", err,
		)
	}
}

var dbLock = sync.Mutex{}

func writeNumber(c *fiber.Ctx) error {
	dbLock.Lock()
	defer dbLock.Unlock()

	time.Sleep(1 * time.Second)

	number := c.Params("number")

	slog.Info("written",
		"number", number,
	)

	return nil
}
