package main

import (
	"context"
	"log/slog"
	"strconv"

	"kafka-parallel-queues/pkg/kafkakit"

	"github.com/segmentio/kafka-go"
)

func main() {
	writer := kafka.NewWriter(kafka.WriterConfig{
		Brokers:   []string{"localhost:29092"},
		Topic:     "numbers",
		BatchSize: 1,
		Balancer:  &kafkakit.SPBalancer{},
	})

	for _, db := range []string{"A", "B", "C"} {
		for i := 1; i <= 3; i++ {
			err := writer.WriteMessages(context.Background(), kafka.Message{
				Key:   []byte(db),
				Value: []byte(strconv.Itoa(i)),
			})

			if err != nil {
				slog.Error("failed to write messages:", "error", err)
			}
		}
	}

	slog.Info("messages sent")
}
