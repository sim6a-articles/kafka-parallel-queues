# Организация параллельных очередей с помощью партиций kafka и каналов go

[Конфигурация "Одна партиция - один обработчик"](./docs/one-partition-one-handler.md)

[Конфигурация "Две партиции - два обработчика"](./docs/two-partitions-two-handlers.md)

[Конфигурация "Две партиции - четыре обработчика"](./docs/two-partitions-four-handlers.md)

[Конфигурация "Две партиции - четыре обработчика - два consumer'а"](./docs/two-partitions-four-handlers-two-consumers.md)

После каждого эксперимента выполняйте

``` sh
docker-compose down -v
```
